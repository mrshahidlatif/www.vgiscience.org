---
layout: page
title: Activities
order: 6
---

## Upcoming activities of VGIscience Priority Program

* [VGIscience lecture series](/lecture-series.html), on regular basis every 2 weeks in 2021, online

## Past activities of VGIscience Priority Program

* VGIscience Young Researchers Seminar, 7.-11. Sep. 2020, online 
* [VGIscience Geovisual Analytics Workshop](http://bdva.net/2018/index.php/vgi-geovisual-analytics-workshop/), 19. Oct. 2018, Konstanz, associated with 4th International Symposium on Big Data Visual and Immersive Analytics ([BDVA](http://bdva.net/2018/))
* [VGIscience Workshop - Platial Analysis](http://platial18.platialscience.net), 20./21. Sep. 2018, Heidelberg
* [VGIscience Collaborative Research Week, 22.-25. May 2018, Heidelberg University](https://hcrw.vgiscience.org/)
* [VGIscience Summer School, 11.-15. Sep. 2017, TU Dresden](https://summerschool.vgiscience.org)

#### co-organsiation through participants of the VGIscience Priority Program

* [Workshop on Multimodal Learning](https://mul-workshop.github.io/), 19th June 2020, associated with [CVPR 2020](http://cvpr2020.thecvf.com/), online* [GIR'19](http://www.geo.uzh.ch/~rsp/gir19/) - 13th Workshop on Geographic Information Retrieval, 28-29 November 2019, Lyon, France
* [PROFILES 2019](https://profiles2019.wordpress.com/) The 6th International Workshop on Dataset PROFlLing and Search, 27. Oct. 2019, Auckland, associated with [18th International Semantic Web Conference](https://iswc2019.semanticweb.org/) 26.-30. Oct. 2019, Auckland, New Zealand
* [Workshop LESSON 2019](http://www.cs.nuim.ie/~pmooney/lesson2019/) - Legal Ethical factorS crowdSourced geOgraphic iNformation, 8.-9. Oct., 2019, Zurich
* [Workshop PLATIAL '19](http://platial19.platialscience.net/) - Interdisciplinary Perspectives on Place, 5.-6. Sep. 2019, Warwick
* [VGI-ALIVE - AnaLysis, Integration, Vision, Engagement](http://k1z.blog.uni-heidelberg.de/2018/01/31/cfp-vgi-alive-workshop-at-agile-2018-lund-sweden/), 12. June 2018, Lund, Sweden, Workshop at [AGILE](https://agile-online.org/index.php/conference/conference-2018)
* [Map Generalisation Practice with Volunteered Geographic Information](http://generalisation.icaci.org/nextevents.html), 12. June 2018, Lund, Sweden, Workshop at [AGILE](https://agile-online.org/index.php/conference/conference-2018)
* [Workshop on Big Data Analytics](https://www.vgiscience.org/2017/11/07/workshop-big-data-analytics-2017.html) with Prof. Dr. Bin Jiang associated with [ILUS: International Land Use Symposium 2017](http://www.ilus2017.ioer.info/)
* [GIR'17 - Workshop on Geographic Information Retrieval](http://www.geo.uzh.ch/~rsp/gir17/), 30 Nov. / 1. Dec. 2017, Heidelberg
