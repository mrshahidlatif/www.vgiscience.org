---
layout: projects
title: Projects
order: 3
project_phase: 2nd_phase
---

# Current Projects

These are the projects we currently work on in the second phase of the VGIscience Priority Programme. You can find [the list of projects in the first phase here](completed-projects.html).

<!-- List of current projects -->
