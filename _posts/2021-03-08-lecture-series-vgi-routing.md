---
layout: post
title: "Lecture Series: VGI-Routing"
date: 2021-03-08 16:56:33+01:00
author: Dirk Burghardt
---

First project presentation within the VGIscience lecture series 2021: 

The [VGI-Routing project](/projects/vgi-routing.html) was presented by Jan-Henrik Haunert und Alex Forsch within the VGIscience lecture series. VGI-Routing aims on inferring personalized routing preferences from voluntarily contributed trajectories.

A video of the overview presentation as well as upcoming dates for the lectures series can be found [here](/lecture-series.html):

<video width="100%" controls>
  <source src="https://kartographie.geo.tu-dresden.de/downloads/vgiscience/lecture-series/vgi-routing/vgi-routing.webm" type="video/webm">
  <source src="https://kartographie.geo.tu-dresden.de/downloads/vgiscience/lecture-series/vgi-routing/vgi-routing.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
