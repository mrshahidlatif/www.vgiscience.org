---
layout: post
title: Workshop on Big Data Analytics with Prof. Dr. Bin Jiang 
date: 2017-11-7 11:45:07 +0200
author:
    - Dirk Burghardt (TU Dresden)
---

Summary
-------
There are two fundamental laws of geography: Scaling law of far more small things than large ones, and Tobler’s law of more or less similar things. Scaling law can also be rephrased as the scaling hierarchy of numerous smallest, a very few largest, and some in between the smallest and the largest. Tobler’s law states that “everything is related to everything else, but near things are related than distant things”. These two laws complement each other and can effectively characterize the Earth’s surface as a fractal or scaling or living structure at different levels of scale. Geospatial big data emerging from the Internet and in particular social media such as OpenStreetMap, Flickr, and Twitter provide a new instrument for verifying these two laws and for better understanding urban structure and dynamics. Big data differs fundamentally from small data in terms of the characteristics such as whether accurately measured or roughly estimated, and whether individual based or aggregated. Big data also differs fundamentally from small data in terms of data analytics both geometrically and statistically. Euclidean geometry has been a default paradigm and it serves as the scientific foundation for various geographic concepts such as distances, directions, and map projections. There is little wonder that current geospatial analysis is essentially Euclidean, dealing with regular shapes and smooth curves. However, geographic features are no doubt irregular and rough, and it implies that Euclidean geometry is not an appropriate paradigm for geography. Instead, fractal geometry, in particular under the third definition of fractal: “a set or pattern is fractal if there are far more small things than large ones in it”, should be adopted for developing penetrating insights into urban structure and dynamics.

In order to see clearly the fractal or scaling or living structure, we must adopt a topological perspective. The topology refers to topological relationship among spatially or geometrically coherent entities such as rivers, mountains, buildings, streets, and cities, rather than that among geometric elements such as pixels, points, lines, and polygons as conceived and used in geographic information systems. Thus scaling and topology are interrelated in the sense that the latter enables us to see the former. This workshop will be organized through a series of lectures, hands-on exercises, and discussions surrounding two major concepts: natural cities and natural streets. Those interested in the workshop are encouraged to download the NaturalCitiesModel and run the enclosed tutorial prior to the workshop. This way we would more time on presentations and discussions. The participants must have their laptops with basic tools including Excel, Axwoman 6.3, and ArcGIS 10.x installed for some hands-on exercises. Internet access is essential for the workshop.

Tools which were used in the workshop
-------------------------------------
Axwoman: [http://fromto.hig.se/~bjg/axwoman/](http://fromto.hig.se/~bjg/axwoman/)

NaturalCitiesModel: [http://www.arcgis.com/home/item.html?id=47b1d6fdd1984a6fae916af389cdc57d](http://www.arcgis.com/home/item.html?id=47b1d6fdd1984a6fae916af389cdc57d)

Head/tail breaks: [http://en.wikipedia.org/wiki/Head/tail_Breaks](http://en.wikipedia.org/wiki/Head/tail_Breaks)

Organisation
------------
The Workshop on Big Data Analytics was organised by the Institue of Cartography (TU Dresden) and the Leibniz Institute of Ecological Urban and Regional Development (IOER). 
(see also [http://www.ilus2017.ioer.info/workshop.html](http://www.ilus2017.ioer.info/workshop.html))
