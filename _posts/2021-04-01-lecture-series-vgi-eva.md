---
layout: post
title: "Lecture Series: Eva VGI II"
date: 2021-04-01 12:00:00+01:00
author:
    - Dirk Burghardt
    - Alexander Dunkel
    - Maximilian Hartmann
---


Eva VGI II status report within the VGIscience lecture series 2021.

The presentation consisted of three parts:

* Overview of Progress (Dirk Burghardt, TUD) – [Slides][d]
* Sunset-sunrise project:
    * Data source, outcomes along the way, visualizations (Alexander Dunkel, TUD) – [Slides][a]
    * Methods: chi and TFIDF, visualizations (Maximilian Hartmann, UZH) – [Slides][m]

A video of the overview presentation as well as upcoming dates for the lectures series can be found [here](/lecture-series.html):

<video width="100%" controls>
  <source src="https://kartographie.geo.tu-dresden.de/downloads/vgiscience/lecture-series/eva-vgi/eva-vgi.webm" type="video/webm">
  <source src="https://kartographie.geo.tu-dresden.de/downloads/vgiscience/lecture-series/eva-vgi/eva-vgi.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>

[d]: https://kartographie.geo.tu-dresden.de/downloads/vgiscience/lecture-series/eva-vgi/eva-vgi-2.pdf
[a]: https://ad.vgiscience.org/lecture-series-evavgi-ii-slides
[m]: https://kartographie.geo.tu-dresden.de/downloads/vgiscience/lecture-series/eva-vgi/sunset-sunrise-202100401.pdf
