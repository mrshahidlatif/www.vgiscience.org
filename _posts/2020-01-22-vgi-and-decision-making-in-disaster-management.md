---
layout: post
title: "VGI and Decision-Making in Disaster Management"
date: "2020-01-22 16:45:37 +0100"
author:
    - Ramian Fathi (Universität Wuppertal)
---

Over the past decade, Social Media data and especially Volunteered
Geographic Information (VGI) have played
an increasingly important role in disaster management. Digital
volunteers, who collaborate with each other
online, have a high potential to improve decision-making by collecting,
assessing, analysing and verifying
valuable, crisis-related information from services such as Twitter,
Facebook or YouTube. Volunteer and Technical
Communities (V&TCs) have grown over time and formed specialised digital
volunteer communities, such
as the Humanitarian OpenStreetMap Team (HOT). Nevertheless, the response
to various recent disasters
reveals a gap between the data created by the V&TCs and the requirements
of the response organizations
and operational decision-makers.

To analyse this in more detail, the Institute for Public Safety and
Emergency Management (University of Wuppertal) and the Humanitarian
Technology Lab (Delft University of Technology) jointly organized the
workshop \"Volunteered Geographic Information (VGI) and Decision-Making
in Disaster Management\". This interdisciplinary workshop was organized
in the context of the research project "Motivation and Participation of
Digital Volunteer Communities in Humanitarian Assistance: Models and
Incentives for Closing the Gap to Decision Makers" and took place on
21st - 22nd January 2020 at the TU Delft Campus in The Hague. The
program was coordinated by Tina Comes and Bartel Van de Walle (TU
Delft), as well as Ramian Fathi and Frank Fiedrich (University of
Wuppertal). Thomas Baar and David Paulus (TU Delft) created the
fictitious scenario and moderated the planned exercises.

![participants](/images/2020-01-22-vgi-and-decision-making-in-disaster-management/image1.jpeg)


The goal of the workshop was to gain insight into and a deeper
understanding of how virtual information flows and products can be
designed to meet the requirements of decision makers (DM) to therefore
increase the use of information, which is provided virtually. The
workshop aimed at identifying the role of Volunteer & Technical
Communities (V&TCs) in supporting decision making in humanitarian
disaster management.

The workshop used a scenario-based setup, where a pandemic disaster
scenario served as the overarching case study. The data and information
was created based on real data from a pandemic and were manipulated in
such a way that no direct conclusion about the disease and affected
places was possible. The sparse database was thus mapped realistically,
whereas the countries in the scenario were fictitious.

Dennis King (US State Department), who gave a keynote speech analysing
various points for cooperation between digital volunteers and
decision-makers, opened the workshop. Further talks were given by
Godfrey Takavarasha and CJ Hendriks (UN OCHA), as well as Emma den Brok
(TU Delft).

![](/images/2020-01-22-vgi-and-decision-making-in-disaster-management/image2.jpeg)
![](/images/2020-01-22-vgi-and-decision-making-in-disaster-management/image3.jpeg)


The two days were divided into three work phases. The first day was
dedicated to understanding the workflows and information sharing and
analysis strategies of the V&TC. On the second day, the focus was on the
interaction between DMs and V&TCs. After a introduction by the V&TC
groups, the DMs were shortly briefed on the scenario and introduced to
their task of allocating resources (treatment units) to the areas
affected. To do so, they had to rely on the material prepared by the
V&TCs on day one.

![](/images/2020-01-22-vgi-and-decision-making-in-disaster-management/image4.jpeg)
![](/images/2020-01-22-vgi-and-decision-making-in-disaster-management/image5.jpeg)


After an update of the crisis situation and the possible global
pandemic, the task continued to be the definition of areas in which
further treatment centres were to be positioned on a regional level. The
working environment of the DMs and V&TCs was spatially separated for
this phase, so that communication via email had to take place. The final
reflection and debriefing made it possible to jointly develop the first
results and highlight the need for further research in the subject
areas. After a first look at the observation protocols and the answers
to the survey handed out to the participants, the scenario exercises
carried out allowed conclusions to be drawn about the workshop
objectives listed above.

![](/images/2020-01-22-vgi-and-decision-making-in-disaster-management/image6.jpeg)
![](/images/2020-01-22-vgi-and-decision-making-in-disaster-management/image7.jpeg)


Most participants noticed the diversity of the information received, but
only a few were able to use or communicate it effectively. It was also
shown that direct communication between the two groups resulted in a
significant benefit. Advantages listed were to learn about needs and
requirements as well as mind-set's and reasoning. In addition, various
forms of communication and structuring approaches were observed, which
allow further conclusions to be drawn about the procedure for
prioritizing goals and the relevance of information management.

<style>
article img {
    width: 49%;
}
article img[alt="participants"] {
    width: inherit;
}
</style>
