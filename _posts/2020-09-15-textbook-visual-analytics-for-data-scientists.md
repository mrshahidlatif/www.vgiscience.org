---
layout: post
title: Textbook "Visual Analytics for Data Scientists" (Springer, 2020)
date: "2020-09-15 11:49:39 +0200"
author:
    - Gennady Andrienko (Fraunhofer IAIS)
---

We would like to announce that our textbook on Visual Analytics for Data Scientists is published by Springer as an eBook and hardcover:

[https://www.springer.com/gp/book/9783030561451](https://www.springer.com/gp/book/9783030561451)

The book is used in our teaching a module on VA for a Data Science MSc at City University London, with more than 500 students successfully graduated during 6 years.

Here is the ToC:

1. Introduction to Visual Analytics by Example
2. General Concepts
3. Principles of Interactive Visualization
4. Computational Techniques in Visual Analytics
5. Visual Analytics for Investigating and Processing Data
6. Visual Analytics for Understanding Multiple Attributes
7. Visual Analytics for Understanding Relationships Between Entities
8. Visual Analytics for Understanding Temporal Distributions and Variations
9. Visual Analytics for Understanding Spatial Distributions and Spatial Variation
10. Visual Analytics for Understanding Phenomena in Space and Time
11. Visual Analytics for Understanding Texts
12. Visual Analytics for Understanding Images and Video
13. Computational Modelling with Visual Analytics
14. Conclusion
