---
layout: post
title: "A place for place - modelling and analysing platial representations"
date: "2019-02-08 22:23:24 +0100"
author:
    - Franz-Benjamin Mocnik
---

Places are understood as locations and areas to which anthropogenic meaning is ascribed. As such, places have been of central interest to philosophers and geographers for a long time, and a large stack of mostly discursive and qualitative literature evolved around this topic. Talking about digital and formal representations of places, however, the inherent vagueness of the aforementioned definition has so far hindered significant progress towards a platial notion of GIS. Place as a concept in the field of GIScience is therefore still in its infancy. Some progress has been made recently, but a consistent theory of how to characterise, represent and utilize places in formal ways is still lacking. A place-based account of GIS and analysis is nevertheless important in the light of the plethora of increasingly place-based information that we have available in an increasingly digital world. Digital technologies are nowadays strongly integrated into everyday life. As a result, a large number of especially urban datasets (e.g., geosocial media feeds, online blogs, etc.) mirror to some degree how people utilise places in subjective and idiosyncratic manners. Taking full advantage of these often user-generated datasets requires a thorough understanding of places. It also makes apparent the pressing need for respective models of representation, analytical approaches, and visualisation methods. This demand reflected by recent events like the [PLATIAL'18 workshop](https://doi.org/10.5281/zenodo.1475269) lays out the motivation of convening this special issue.

We are seeking your original contributions on the following topics (and beyond if fitting):

* How can we move forward the integration of platial information with GIS?
* How can we integrate and align GIScience notions of place with existing human-geographic and philosophical notions?
* How is it possible to establish and quantify relationships between adjacent places?
* What might be a suitable strategy for aggregating subjective platial information?
* What roles do uncertainty and fuzziness take in a platial theory of geoinformation?
* In which ways can places be visualised, and how can we do that at multiple scales?
* What can we learn about places from volunteered and ambient geographic information?
* How can platial analysis be integrated with applied research agendas from neighbouring disciplines like sociology, urban planning, or human geography?
* (Further topics are welcome if they fit the overall theme of this workshop.)

## Important dates and anticipated timeline

| 30 March 2019 | Deadline: Full paper submission |
| 15 June 2019 | Anticipated paper acceptance notification |
| 1 July 2019 | Camera-ready papers are due |
| 1 August (anticipated) | Publication of the special issue |

If you are not sure whether or not your contribution might be suitable for this issue, you are welcome to contact one of the guest editors listed below in advance with questions about your possible submission (e.g., by sending us a brief abstract).

## Guest editors

[Rene WESTERHOLT, University of Warwick](mailto:rene.westerholt@warwick.ac.uk)  
[Franz-Benjamin MOCNIK, Heidelberg University](mailto:mocnik@uni-heidelberg.de)  
[Alexis COMBER, University of Leeds](mailto:a.comber@leeds.ac.uk)  
[Clare DAVIES, University of Winchester](mailto:clare.davies@winchester.ac.uk)  
[Dirk BURGHARDT, TU Dresden](mailto:dirk.burghardt@tu-dresden.de)  
[Alexander ZIPF, Heidelberg University](mailto:zipf@uni-heidelberg.de)  
