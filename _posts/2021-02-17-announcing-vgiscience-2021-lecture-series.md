---
layout: post
title: "Announcing VGIscience 2021 lecture series"
date: "2021-02-17 15:25:35 +0100"
author:
    - Dirk Burghardt (TU Dresden)
---

The VGIscience priority program will hold a lecture series in which all research
projects will provide an overview talk of around 10 minutes, followed by a more
detailed presentation and discussion of selected research questions.

The first presentation on the VGI Routing project will take place on March 4th
at 1:00 p.m. [See the schedule](/lecture-series.html) for all lectures.
