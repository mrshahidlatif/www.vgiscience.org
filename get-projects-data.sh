#!/usr/bin/env bash

# fail on errors
set -o errexit -o nounset -o pipefail

# require api token
test -n "$GITLAB_API_TOKEN"

# define default variables if not set in environment
: "${API_URL:="https://gitlab.vgiscience.de/api/v4"}"
: "${CURL_OPTIONS:="--silent"}"
: "${DATA_DIR="$PWD/_data"}"
: "${DATA_FILE="$DATA_DIR/projects.json"}"
: "${BIB_DIR="$PWD/_bibliography"}"
: "${BIB_FILE="$BIB_DIR/projects.bib"}"

# create destinations
mkdir -p "$DATA_DIR"
echo -n >"$DATA_FILE"
mkdir -p "$BIB_DIR"
echo -n >"$BIB_FILE"

# get group IDs from project files
mapfile -t project_ids < <(
    yq eval '.project_ids[]' _config.yml
)

# parse data from GitLab API for every group
for project_id in "${project_ids[@]}"; do

    # verbose command line output
    echo -n "getting data for project $project_id: "

    # get project info
    project_json=$(
        curl "$CURL_OPTIONS" \
            --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" \
            "$API_URL/projects/$project_id" |
            jq '. | {
                "project_id": .id,
                "acronym": .name,
                "slug": .path,
                "image": .avatar_url,
                "description": .description,
                "base_url": .web_url,
                "branch": .default_branch,
                "group_id": .namespace.id,
                "topics": .tag_list
            }'
    )

    echo -n "$(echo "$project_json" | jq '.acronym')"

    # isolate some variables from project json
    group_id=$(
        echo "$project_json" | jq -r '.group_id'
    )
    base_url=$(
        echo "$project_json" | jq -r '.base_url'
    )
    branch=$(
        echo "$project_json" | jq -r '.branch'
    )
    slug=$(
        echo "$project_json" | jq -r '.slug'
    )

    # add raw image path to base url and escape slashes to make sed happy
    base_url=$(echo "$base_url/-/raw/$branch" | sed 's|\/|\\\/|g')

    # get project's readme content and replace relative with absolute paths
    echo -n ", readme"
    readme_path="README%2Emd/raw?ref=master"
    readme=$(
        curl "$CURL_OPTIONS" \
            --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" \
            "$API_URL/projects/$project_id/repository/files/$readme_path" |
            sed "/http/! s/\[\(.*\)\](\(.*\))/[\1](${base_url}\/\2)/g"
    )

    # get project's bibliography and store it in local file for jekyll-scholar
    echo -n ", bibliography"
    bibliography_path="_bibliography%2Freferences%2Ebib/raw?ref=master"
    bibliography=$(
        curl "$CURL_OPTIONS" \
            --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" \
            "$API_URL/projects/$project_id/repository/files/$bibliography_path"
    )
    echo "$bibliography" >"$BIB_DIR/$slug.bib"

    # append project's bibliography to global bibliography file
    echo "$bibliography" >>"$BIB_FILE"

    # append readme to project json object
    project_json=$(
        echo "$project_json" |
            jq --arg README "$readme" '. | {
                "project_id": .project_id,
                "acronym": .acronym,
                "slug": .slug,
                "image": .image,
                "description": .description,
                "topics": .topics,
                "readme": $README,
            }'
    )

    # get project member ids
    mapfile -t brutto_project_members_ids < <(
        # get actual project members
        curl "$CURL_OPTIONS" \
            --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" \
            "$API_URL/projects/$project_id/members" |
            jq '.[] | .id'
        # get overlying group members
        curl "$CURL_OPTIONS" \
            --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" \
            "$API_URL/groups/$group_id/members" |
            jq '.[] | .id'
    )

    # remove duplicate entries (sort unique)
    mapfile -t project_members_ids < <(
        echo "${brutto_project_members_ids[@]}" |
            tr ' ' '\n' |
            sort -u
    )

    echo -n ", members"

    # get member info for each namespace group member id
    for member_id in "${project_members_ids[@]}"; do

        # get member info
        member_json=$(
            curl "$CURL_OPTIONS" \
                --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" \
                "$API_URL/users/$member_id" |
                jq '{
                    id,
                    name,
                    job_title,
                    organization,
                    location,
                    avatar_url,
                    website_url
                }'
        )

        echo -n "."

        # concatenate member object with former member objects
        members_json+=$member_json
    done

    # combine concatenated objects to one valid json object
    members_json=$(
        echo "$members_json" | jq --slurp '{ members: map_values( . ) }'
    )

    # add group members to project object
    project_json=$(
        echo "$project_json $members_json" | jq --slurp add
    )

    # empty var for next loop
    unset members_json

    # concatenate project object with former project objects
    projects_json+=$project_json

    echo
done

# combine concatenated objects to one valid json object and
# create Jekyll data file, see https://jekyllrb.com/docs/datafiles/
echo "$projects_json" | jq --slurp >"$DATA_FILE"
