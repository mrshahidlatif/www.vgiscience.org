# Welcome to the VGIscience Website Project

This repository contains the content of the [VGIscience website](https://www.vgiscience.org).

What can you do with it?

* [Create blog posts](#create-blog-posts)
* [Manage project content](#manage-project-content)
* [Manage publications](#manage-publications)
* [Manage project members](#manage-project-members)
* [Develop locally](#develop-locally)

What do you need to keep in mind?

* [Staging vs. Production](#staging-vs-production)
* [Scheduled builds](#scheduled-builds)

## How to start

* The easy way: use the `Web IDE` button [above the file list](#) on the right next to the blue `Clone` button.
* The hard way: clone the repository to your local device, create a new branch, edit and build locally (see [below](#develop-locally)), push your changes to a new remote branch and create a merge request.

## Create blog posts

Create news or blog posts by adding files to the `_posts` directory. Use the format `yyyy-mm-dd-title-of-post.md` without spaces, caps or umlauts for the file name.

Start the file with the following YAML-frontmatter:

    ---
    layout: post
    title: "The title of your Post"
    date: 2018-02-12 12:14
    author:
        - First Name Last Name (Department, University)
        - More authors, if any
    ---

When you hit the green `Commit changes` button, the website files will be rebuilt. It takes a moment for the build process to finish. You can watch it on the [jobs page][j] (*CI/CD* -> *Jobs* in the menu on the left).

[j]: https://gitlab.vgiscience.de/www/www.vgiscience.org/-/jobs

## Staging vs. Production

Your changes to the website will *not* immediately land on the production website. Instead, they go to [www**.staging.**vgiscience.org][st] after your commit, where you should examine them first, and adjust them if needed. Then use the green `Submit merge request` button to create a [merge request][mr]. Once your changes are merged to the production branch, they will be deployed to [www.vgiscience.org][pr].

[st]: https://www.staging.vgiscience.org
[pr]: https://www.vgiscience.org
[mr]: https://gitlab.vgiscience.de/help/user/project/merge_requests/index.md

## Manage project content

The content for the individual VGIscience projects is *not* managed through this repository directly. Instead, it is automatically parsed during build-time from the `README.md` of [each projects dedicated GitLab repository][ps].

Each project's `README.md` consists of the full project description text, written in [Markdown][md]. Any included image files or other related assets are placed in the repository as well. If you would like to change or extend the project pages on the website, go ahead and edit the `README.md` of your project.

If a project has an avatar image set-up, it will be shown prominently on the project page as an iconic image, see e.g. [EVA-VGI 2 project page][ev]. You are encouraged to add it to improve overall web page impressions.

[ps]: https://gitlab.vgiscience.de/explore/projects?tag=project&non_archived=true&sort=name_asc
[md]: https://en.wikipedia.org/wiki/Markdown
[ev]: https://www.vgiscience.org/projects/eva-vgi-2.html

## Manage publications

Project-related publications are managed, similarly to the description of a project, in a dedicated file in each project repository. There is a directory `_bibliography`, that includes a file `references.bib`. In this file, project-related publications should be listed using [BibTeX][bt] notation.

During build-time of the website, this file is parsed. All publications will be listed both at the bottom of the project page, as well as the general [VGIscience publications page][pp].

Publications will be sorted by year as well as by type. You should adhere to [BibTeX entry types nomenclature][et] to make sure they are placed correctly.

In addition, publications can also be entered in the external publication management service [Bibsonomy][bs]. Your Bibsonomy user account must be a member of the group [vgiscience][bg] and your publication must be marked "vgiscience" with the `relevant for` system tag. Nevertheless, publications added solely to Bibsonomy will only be shown on the general publications page, but not at the bottom of each individual project page. Therefore the recommended way is to add them to the BibTeX file.

[bt]: https://en.wikipedia.org/wiki/BibTeX
[pp]: https://www.vgiscience.org/publications.html
[et]: https://en.wikipedia.org/wiki/BibTeX#Entry_types
[bs]: https://www.bibsonomy.org
[bg]: https://www.bibsonomy.org/group/vgiscience

## Manage project members

Making use of GitLab repositories to map VGIscience projects, it is obvious to also manage project membership within the project repositories. GitLab user accounts are associated with project repositories through membership. Every GitLab user, who is a member of a project, will also be shown as a project member on the VGIscience website. The data is also read and processed during build-time of the website.

Members are encouraged to add the following details to their [GitLab user profiles]:

* full name which they want to appear with on the website
* picture (avatar)

Further profile information is entirely voluntary and only shown if provided:
* job title (e.g. Dr., MSc, etc.)
* organization (e.g. university department)
* location (e.g. city of residence)
* website - if set, a link to the website will be placed behind the full name

[gp]: https://gitlab.vgiscience.de/-/profile

## Scheduled builds

If not triggered manually, the website [is scheduled][sh] to automatically update every evening at 17:00 UTC. This means, your changes to individual project content, publications or membership will not be visible on the website until shortly after then (unless a build is triggered either manually or by pushing/merging to the production branch).

[sh]: https://gitlab.vgiscience.de/www/www.vgiscience.org/-/pipeline_schedules

## Develop locally

Use Docker for local development.

1. Build Docker image:

        docker build -t registry.gitlab.vgiscience.org/www/www.vgiscience.org:latest .

2. Use Docker image environment to download the project data from the GitLab API (analog `bash ./get-bibsonomy-data`):

        docker run \
            --name "www.vgiscience.org_get-projects-data" \
            --env GITLAB_API_TOKEN=$(gopass show vgiscience/gitlab.vgiscience.de/access_tokens/www.vgiscience.org/read_api.read_repository) \
            --volume="$PWD:/srv/jekyll" \
            --interactive \
            --tty \
            --rm \
            registry.gitlab.vgiscience.org/www/www.vgiscience.org:latest \
            bash ./get-projects-data.sh

3. Use Docker image environment to run a local copy of the website served by Jekyll:

        docker run \
            --name "www.vgiscience.org_dev" \
            --publish=4000:4000 \
            --volume="$PWD:/srv/jekyll" \
            --volume="$PWD/vendor/bundle:/usr/local/bundle" \
            --interactive \
            --tty \
            --rm \
            registry.gitlab.vgiscience.org/www/www.vgiscience.org:latest \
            bundle exec jekyll serve --host=0.0.0.0
