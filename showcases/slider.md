---
layout: image-slider-iframe
slides:
- image: /showcases/tagmaps.jpg
  link: /showcases/tagmaps.html
  text: Spatio-Temporal Tag and Photo Location Clustering for generating Tag Maps
- image: /showcases/privacy-aware.png
  link: /showcases/privacy-aware.html
  text: Privacy-Aware Visualization of Volunteered Geographic Information (VGI)
- image: /showcases/glyph-miner.jpg
  link: /showcases/glyph-miner.html
  text: Glyph Miner, a system for extracting glyphs from early typeset prints
- image: /showcases/enap.jpg
  link: /showcases/enap.html
  text: Dataset with images related to the Central Europe Floods in May/June 2013
- image: /showcases/lineman.jpg
  link: /showcases/lineman.html
  text: Tool for aligning GeoJSON LineStrings to bitmap images such as old maps
- image: /showcases/clipgeo.jpg
  link: /showcases/clipgeo.html
  text: Large Spatial Point Dataset Extraction and Visualization
---
