---
layout: home
---

<div style="border:0;width:100%;height:0;padding-bottom:56.25%;position:relative;margin-bottom:3em">
<iframe src="/showcases/slider.html" scrolling="no" style="border:0;position:absolute;width:100%;height:100%"></iframe>
</div>

## Volunteered Geographic Information Research Programme

Welcome to our site! We are a group of VGI enthusiasts from several research institutions. Our goal is to combine the work of our projects to get a greater overall idea about volunteered geographic information.


This research project is funded by German Research Foundation as [Priority Programme 1894](https://www.dfg.de/foerderung/info_wissenschaft/2018/info_wissenschaft_18_32/index.html).

![DFG](./assets/images/dfg-logo.svg)

---
