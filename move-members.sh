#!/usr/bin/env bash

# define default variables if not set in environment
: "${GITLAB_API_TOKEN:=$(gopass show www/gitlab.vgiscience.de/ml/access_tokens/sudo_api)}"
: "${API_URL:="https://gitlab.vgiscience.de/api/v4"}"
: "${CURL_OPTIONS:="--silent"}"


# get group IDs from project files
mapfile -t project_ids < <(
    yq read _config.yml 'project_ids.*'
)


# set members group access levels
for group_id in $(

    # get group IDs
    for project_id in "${project_ids[@]}"; do
        curl "$CURL_OPTIONS" \
            --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" \
            "$API_URL/projects/$project_id" |
                jq '.namespace | .id'

    done
); do

    echo "---- Group $group_id -----"

    for member_id in $(
        # get group members
        curl "$CURL_OPTIONS" \
            --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" \
            "$API_URL/groups/$group_id/members" |
                jq '.[] | .id'
    ); do

        # set members group access level
        curl "$CURL_OPTIONS" \
            --request PUT \
            --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" \
            --data "access_level=30" \
            "$API_URL/groups/$group_id/members/$member_id" |
                jq -r '"\(.name), \(.access_level)"'

        # add member to groupprojects

        for groupproject_id in $(
            # get group projects
            curl "$CURL_OPTIONS" \
                --fail \
                --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" \
                "$API_URL/groups/$group_id" |
                    jq '.projects[] | .id'
        ); do
            # add member to group project
            curl "$CURL_OPTIONS" \
                --request POST \
                --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" \
                --data "user_id=$member_id&access_level=40" \
                "$API_URL/projects/$groupproject_id/members" |
                    jq -r '"\(.name), \(.access_level)"'

        done # end groupproject_id

    done # end member_id

done
