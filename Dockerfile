FROM jekyll/jekyll:4

RUN apk add --update --no-cache curl jq

RUN apk add \
    --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community \
    yq

COPY --chown=jekyll:jekyll Gemfile /srv/jekyll/Gemfile
COPY --chown=jekyll:jekyll Gemfile.lock /srv/jekyll/Gemfile.lock

RUN bundle install
