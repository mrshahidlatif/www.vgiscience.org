---
layout: projects
project_phase: 1st_phase
---


# Completed Projects

These are the projects we worked on in the first phase of the VGIscience Priority Programme. You can find [the list of current projects here](projects.html).

<!-- List of completed projects -->
