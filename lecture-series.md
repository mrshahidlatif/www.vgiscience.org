---
layout: page
---

# VGIscience 2021 lecture series

In order to emphasize the collaborative progress of our current research work,
the *VGIscience 2021 lecture series* will take place this year, each carried out
as a [live online video conference][2].

Every other week on Thursday at 13:00, another VGIscience project group will
introduce their research project and highlight already achieved results of their
work in a 10-minute overview presentation. Following that a current research
question of the project will be presented more in detail in a 15-minute
presentation with a chance for the audience to ask questions and discuss the
topic afterwards.

The 10-mintue overview presentation will be recorded and made publicly available
afterwards on the VGIscience website.

## Preliminary schedule

| Date       | Time  | Project                | Topic                                                                                                                   | Links            |
| ---------- | ----- | ---------------------- | ----------------------------------------------------------------------------------------------------------------------- | ---------------- |
| 2021-03-04 | 13:00 | [VGI-Routing]          | Inferring personalized multi-criteria routing models from sparse sets of voluntarily contributed trajectories           | [Video][1]       |
| 2021-03-18 | 13:00 | [COVMAP]               | Continuation of Comprehensive Conjoint GPS, Sensor and Video Data                                                       | [Video][2]       |
| 2021-04-01 | 13:00 | [EVA-VGI]              | Geovisual analysis of VGI for understanding people’s behaviour in relation to multi-faceted context                     | [Video][3]       |
| 2021-04-15 | 13:00 | [IDEAL-VGI]            | Information Discovery from Big Earth Observation Data Archives by Learning from Volunteered Geographic Information      | [Video][4]       |
| 2021-04-29 | 13:00 | [Trajectories]         | Dynamic and Customizable Exploitation of Trajectory Data                                                                | [Video][5]       |
| 2021-05-20 | 13:00 | [AQA]                  | Algorithmic Quality Assurance: Theory and Practice                                                                      | [Video][6]       |
| 2021-05-27 | 13:00 | [WorldKG]              | World-Scale Completion of Geographic Knowledge                                                                          | tba              |
| 2021-06-10 | 13:00 | [TOVIP]                | Improvement of Task-Oriented Visual Interpretation of VGI Point Data                                                    | [Video][8]       |
| 2021-06-24 | 13:00 | [VA4VGI]               | Visual analysis of volunteered geographic information for interactive situation modeling and real-time event assessment | [Video][9]       |
| 2021-07-08 | 13:00 | [ExpoAware]            | Environmental volunteered geographic information for personal exposure awareness and healthy mobility behaviour         | [Conference][10] |
| 2021-07-22 | 13:00 | [Privacy Aspects]      | Privacy Aspects in Volunteered Geographic Information                                                                   |                  |
| 2021-08-05 | 13:00 | [DVCHA]                | Distributed Decision Making and its Impact on Disaster Management Organisations                                         |                  |
| 2021-08-19 | 13:00 | [Landmark Uncertainty] | The Effects of Landmark Uncertainty in VGI-based Maps: Approaches to Improve Wayfinding and Navigation Performance      |                  |
| 2021-09-02 | 13:00 | [BirdTrace]            | Uncertainty-Aware Enrichment of Animal Movement Trajectories by VGI                                                     |                  |
| 2021-09-16 | 13:00 | [vgiReports]           | Accessible Reporting of Spatiotemporal Geographic Information Leveraging Generated Text and Visualization               |                  |

[1]: https://kartographie.geo.tu-dresden.de/downloads/vgiscience/lecture-series/vgi-routing/vgi-routing.html
[2]: https://kartographie.geo.tu-dresden.de/downloads/vgiscience/lecture-series/covmap/covmap.html
[3]: https://kartographie.geo.tu-dresden.de/downloads/vgiscience/lecture-series/eva-vgi/eva-vgi.html
[4]: https://kartographie.geo.tu-dresden.de/downloads/vgiscience/lecture-series/ideal-vgi/ideal-vgi.html
[5]: https://kartographie.geo.tu-dresden.de/downloads/vgiscience/lecture-series/trajectories/trajectories.html
[6]: https://kartographie.geo.tu-dresden.de/downloads/vgiscience/lecture-series/aqa/aqa.html
[8]: https://kartographie.geo.tu-dresden.de/downloads/vgiscience/lecture-series/tovip/tovip.html
[9]: https://kartographie.geo.tu-dresden.de/downloads/vgiscience/lecture-series/va4vgi/va4vgi.html
[10]: https://tu-dresden.zoom.us/j/84899787097?pwd=b2RsZmIwS3BPamZLbC96YXVJOVRxUT09

[VGI-Routing]: https://www.vgiscience.org/projects/vgi-routing.html
[COVMAP]: https://www.vgiscience.org/projects/covmap-2.html
[EVA-VGI]: https://www.vgiscience.org/projects/eva-vgi-2.html
[IDEAL-VGI]: https://www.vgiscience.org/projects/ideal-vgi.html
[Trajectories]: https://www.vgiscience.org/projects/trajectories-2.html
[AQA]: https://www.vgiscience.org/projects/aqa.html
[WorldKG]: https://www.vgiscience.org/projects/worldkg.html
[TOVIP]: https://www.vgiscience.org/projects/tovip.html
[VA4VGI]: https://www.vgiscience.org/projects/va4vgi-2.html
[ExpoAware]: https://www.vgiscience.org/projects/expoaware.html
[Privacy Aspects]: https://www.vgiscience.org/projects/privacy-aspects.html
[DVCHA]: https://www.vgiscience.org/projects/dvcha-2.html
[Landmark Uncertainty]: https://www.vgiscience.org/projects/lm-uncertainty.html
[BirdTrace]: https://www.vgiscience.org/projects/birdtrace.html
[vgiReports]: https://www.vgiscience.org/projects/vgireports.html

<style>
    table {
        width: 100%;
    }
    th {
        text-align: left;
    }
    table td {
        vertical-align: baseline;
        padding: 1em 1em 0 0;
    }
    table td:nth-child(4) {
        white-space: break-spaces;
    }
</style>
