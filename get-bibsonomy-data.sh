#!/usr/bin/env bash

# fail on errors
set -euo pipefail

: "${BIBSONOMY_API_KEY:=$(gopass show vgiscience/bibsonomy.org/api/vgscnc)}"
: "${BIBSONOMY_API_USER:=vgscnc}"
: "${BIBSONOMY_API_URL:="https://www.bibsonomy.org/api/posts?resourcetype=bibtex&format=bibtex&tags=sys:relevantfor:vgiscience&start=0&end=999"}"
: "${BIB_DIR="$PWD/_bibliography"}"
: "${BIB_FILE="$BIB_DIR/bibsonomy.bib"}"

# create destinations
mkdir -p "$BIB_DIR"
echo -n >"$BIB_FILE"

# read publication list from bibsonomy
bibsonomy_bibtex=$(
    curl \
        --fail \
        --silent \
        --user "$BIBSONOMY_API_USER:$BIBSONOMY_API_KEY" \
        "$BIBSONOMY_API_URL"
)

# place the publication list into the bib file
echo "$bibsonomy_bibtex" >"$BIB_FILE"
