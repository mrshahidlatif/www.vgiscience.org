---
layout: page
title: Publications
order: 3
scholar:
  group_by: type, year
  group_order: descending
  remove_duplicates: true
---

{% bibliography --file references --file projects --file bibsonomy %}
